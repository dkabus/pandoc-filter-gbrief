# LaTeX DIN letter Pandoc filter

This Pandoc filter converts the file to a DIN letter
using the gbrief LaTeX package and given metadata.

Simply add metadata using yaml to the header of your document
and convert using pandoc applying the lua filter.

Supported metadata fields:

- `documentclass`: must be set to "letter", "brief" or "gbrief"
- `title`: the subject of the letter
- `author`: sender
- `date`: date
- `lang`: language (tested with `de` and `en`)
- `to`: address of the recipient (multi line)
- `from`: address of the author (multi line)
- `fromwindow`: address shown in the window (one line)
- `phones`: list of phone numbers (multi line)
- `internet`: list of email addresses, websites, etc (multi line)
- `opening`: greeting at the beginning of the document (one line)
- `closing`: greeting at the end of the document (one line)
- `options`: list of switches for extra markings
	(`lochermarke`, `faltmarken`, `fenstermarken`, `trennlinien`)

## Files

- [`default.yaml`](default.yaml): Metadata to use for letters. Edit this file!
- [`example.md`](example.md): Markdown source file. Edit this file!
- [`example.tex`](example.tex): LaTeX code from compiling the Markdown file via Pandoc.
- [`example.pdf`](example.pdf): Resulting PDF file from compiling the LaTeX file.

## Compilation via Gitlab CI

This repository contains configuration to automatically compile the document
and publish it via Gitlab Pages. You can find the pages here:

<https://dkabus.gitlab.io/pandoc-filter-gbrief/>

<https://gitlab.com/dkabus/pandoc-filter-gbrief/>

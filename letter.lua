--[[
This Pandoc filter converts the file to a DIN letter
using the gbrief LaTeX package and given metadata.
--]]

function str(obj)
    if type(obj) == type("") then
        return obj
    else
        return pandoc.utils.stringify(obj)
    end
end

function raw(code)
	return pandoc.MetaBlocks(pandoc.RawBlock("latex", code))
end

local zeilenlabels = {"A","B","C","D","E","F"}
function zeilenweise(rows, prefix, labels)
	labels = labels or zeilenlabels
	local s = ""
	local I = (#rows<#labels and #rows or #labels)
	for i=1,I do
		s = s.."\\"..prefix..labels[i].."{"..str(rows[i]).."}"..(i==I and "" or "\n")
	end
	return s
end

function cat(list, sep)
	sep = sep or ""
	local s = ""
	for i, l in ipairs(list) do
		s = s..(i==1 and "" or sep)..str(l)
	end
	return s
end

function cmd(cmd, args)
	return "\\"..str(cmd).."{"..cat(args or {}, "}{").."}"
end

function renew(cmd, def)
	return "\\renewcommand{\\"..str(cmd).."}[0]{"..(def or "").."}"
end

function cmds(lines)
	local t = {}
	for i, line in ipairs(lines) do
		t[i] = cmd(line)
	end
	return cat(t, "\n")
end

function merge_into(tab, key, content)
	if tab[key] then
		tab[key] = {tab[key], content}
	else
		tab[key] = content
	end
end

function contains(tab, value)
	for k, v in pairs(tab) do
		if v == value then return true end
	end
	return false
end

function brief(m)
	if contains({"gbrief2", "gbrief", "brief", "letter"}, str(m["documentclass"] or ""):lower():gsub("-", "")) then
		m["documentclass"] = "g-brief2"

		if m["lang"] and str(m["lang"]):sub(1,2) == "en" then
			merge_into(m, "classoption", "english")
		end

		merge_into(m, "header-includes", raw([[
		\let\titleold\title
		\renewcommand{\title}[1]{\titleold{#1}\Betreff{#1}}
		\let\authorold\author
		\renewcommand{\author}[1]{\authorold{#1}\Name{#1}\Unterschrift{#1}}
		\let\dateold\date
		\renewcommand{\date}[1]{%
			\def\temp{#1}\ifx\temp\empty%
				\dateold{\today}%
				\Datum{\today}%
			\else%
				\dateold{#1}%
				\Datum{#1}%
			\fi
		}
		\renewcommand{\textsc}[1]{#1}
		]]))

		local address = {m.author, table.unpack(m.from or {})}
		merge_into(m, "include-before", raw(cat({
			m.from and zeilenweise(address, "AdressZeile") or "",
			m.fromwindow and cmd("RetourAdresse", {str(m.fromwindow)})
			or m.from and cmd("RetourAdresse", {cat(address, " \\textbullet ")}) or "",
			m.to and cmd("Adresse", {cat(m.to, " \\\\ ")}) or "",
			m.phones and zeilenweise(m.phones, "TelefonZeile") or "",
			m.internet and zeilenweise(m.internet, "InternetZeile") or "",
			m.opening and cmd("Anrede", {str(m.opening)}) or "",
			m.closing and cmd("Gruss", {str(m.closing), "0mm"}) or "",
			m.options and cmds(m.options) or "",
			m.refno and cmd("IhrZeichen", {str(m.refno)})
				or renew("ihrzeichentext", ""),
			m.refmy and cmd("MeinZeichen", {str(m.refmy)})
				or cat({renew("meinzeichentext", ""), renew("unserzeichentext", "")}),
			m.refdate and cmd("IhrSchreiben", {str(m.refdate)})
				or renew("ihrschreibentext", ""),
			"\\begin{g-brief}"
		},"\n")))

		merge_into(m, "include-after", raw([[\end{g-brief}]]))

		return m
	end
end

return {{Meta = brief}}

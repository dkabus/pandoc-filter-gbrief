md := $(wildcard *.md)

all: $(md:md=pdf)

%.pdf: %.tex
	latexmk -quiet -aux-directory=.latexmk -pdflua -interaction=nonstopmode -f -time $<

%.tex: %.md default.yaml letter.lua letter.sed
	pandoc -s \
		--metadata-file=$(word 2,$^) \
		--lua-filter=$(word 3,$^) \
		-o $@ $<
	grep -q 'g-brief2' $@ && sed -i -f $(word 4,$^) $@ || :

clean:
	$(RM) -r .latexmk
